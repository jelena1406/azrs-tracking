# Azrs Tracking



## Projekat u okviru kursa Alati za razvoj softvera

### Alati koje sam koristila:
1. Git
2. CMake
3. Valgrind
4. GCov
5. ClangFormat
6. ClangTidy
7. Docker
8. Cppcheck
9. Doxygen
10. GDB

### Alati koje nisam uspela da iskoristim na projektu:
1. Gammaray
